﻿using System.Collections;
using System.Collections.Generic;
//using System.Runtime.Hosting;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    public static bool gameIsPaused = false;
    [SerializeField] GameObject PausePanel;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused == true)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }
    public void ResumeGame()
    {
        PausePanel.SetActive(false);
        Time.timeScale = 1;
        gameIsPaused = false;
    }
    public void PauseGame()
    {
        PausePanel.SetActive(true);
        Time.timeScale = 0;
        gameIsPaused = true;
    }
    public void GameQuit()
    {
        UnityEngine.Debug.Log("Quit");
        Application.Quit();
    }
}

