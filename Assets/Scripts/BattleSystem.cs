﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Diagnostics;

public enum BattleState { START, YVONNATURN, SHENTURN, RUTHTURN, ENEMYTURN, WIN, LOSE }
public class BattleSystem : MonoBehaviour
{
    public BattleState state;
    public BattleHud hud;
    public TextMeshProUGUI eventText;
    public GameObject player;

    public Character yvonna;
    public Character shen;
    public Character ruth;
    public Character enemy;

    public int currentTurn = 0;
    public string[] turnOrder = new string[4];

    public void BeginBattle()
    {
        player.SetActive(false);
        ResetStats();
        hud.setEName(enemy);
        hud.setHP(yvonna, shen, ruth, enemy);
        hud.setMP(yvonna, shen, ruth);
        hud.backgroundPanel.SetActive(true);
        hud.fightHud.SetActive(true);

        StartCoroutine(SetTurnOrder());
    }
    public void AttackButton()
    {
        if (state != BattleState.YVONNATURN && state != BattleState.SHENTURN && state != BattleState.RUTHTURN)
            return;

        if (state == BattleState.YVONNATURN)
        {
            StartCoroutine(playerCharacterAttack(yvonna));
        }
        else if (state == BattleState.SHENTURN)
        {
            StartCoroutine(playerCharacterAttack(shen));
        }
        else if (state == BattleState.RUTHTURN)
        {
            StartCoroutine(playerCharacterAttack(ruth));
        }
    }
    public IEnumerator EnemyAttack()
    {
        yield return new WaitForSeconds(1.5f);
        int attacking = UnityEngine.Random.Range(1, 4);

        // If the random number is a 1 and Yvonna is still alive, Yvonna is attacked. If Yvonna is dead the enemy goes back and generates a new number.
        if (attacking == 1 && yvonna.isAlive == true)
        {
            StartCoroutine(enemyCharacterAttack(yvonna));
        }
        else if (attacking == 1 && yvonna.isAlive == false)
            StartCoroutine(EnemyAttack());

        // If the random number is a 2 and Shen is still alive, Shen is attacked. If Shen is dead the enemy goes back and generates a new number.
        if (attacking == 2 && shen.isAlive == true)
        {
            StartCoroutine(enemyCharacterAttack(shen));
        }
        else if (attacking == 2 && shen.isAlive == false)
            StartCoroutine(EnemyAttack());

        // If the random number is a 3 and Ruth is still alive, Ruth is attacked. If Ruth is dead the enemy goes back and generates a new number.
        if (attacking == 3 && ruth.isAlive == true)
        {
            StartCoroutine(enemyCharacterAttack(ruth));
        }
        else if (attacking == 3 && ruth.isAlive == false)
            StartCoroutine(EnemyAttack());
    }
    public IEnumerator SetTurnOrder()
    {
        // Generates a 20 sided dice roll for each character
        int i = 0;
        int yvRoll = UnityEngine.Random.Range(1, 21);
        int shRoll = UnityEngine.Random.Range(1, 21);
        int ruRoll = UnityEngine.Random.Range(1, 21);
        int enRoll = UnityEngine.Random.Range(1, 21);
        yield return new WaitForSeconds(1f);
        // Determines the battle order by adding the roll to the character's initiative score and sorting from highest to lowest
        while (i < 4)
        {
            if ((yvonna.initiative + yvRoll) >= (shen.initiative + shRoll) && (yvonna.initiative + yvRoll) >= (ruth.initiative + ruRoll) && (yvonna.initiative + yvRoll) >= (enemy.initiative + enRoll) && yvonna.turnInBattle == false)
            {
                eventText.text = "Yvonna Initiative: " + (yvonna.initiative + yvRoll);
                yield return new WaitForSeconds(1.5f);
                turnOrder[i] = "Yvonna";
                yvonna.turnInBattle = true;
                yvRoll = 0;
            }
            else if ((shen.initiative + shRoll) >= (yvonna.initiative + yvRoll) && (shen.initiative + shRoll) >= (ruth.initiative + ruRoll) && (shen.initiative + shRoll) >= (enemy.initiative + enRoll) && shen.turnInBattle == false)
            {
                eventText.text = "Shen Initiative: " + (shen.initiative + shRoll);
                yield return new WaitForSeconds(1.5f);
                turnOrder[i] = "Shen";
                shen.turnInBattle = true;
                shRoll = 0;
            }
            else if ((ruth.initiative + ruRoll) >= (yvonna.initiative + yvRoll) && (ruth.initiative + ruRoll) >= (shen.initiative + shRoll) && (ruth.initiative + ruRoll) >= (enemy.initiative + enRoll) && ruth.turnInBattle == false)
            {
                eventText.text = "Ruth Initiative: " + (ruth.initiative + ruRoll);
                yield return new WaitForSeconds(1.5f);
                turnOrder[i] = "Ruth";
                ruth.turnInBattle = true;
                ruRoll = 0;
            }
            else if ((enemy.initiative + enRoll) >= (yvonna.initiative + yvRoll) && (enemy.initiative + enRoll) >= (shen.initiative + shRoll) && (enemy.initiative + enRoll) >= (ruth.initiative + ruRoll) && enemy.turnInBattle == false)
            {
                eventText.text = enemy.fullName + " Initiative: " + (enemy.initiative + yvRoll);
                yield return new WaitForSeconds(1.5f);
                turnOrder[i] = "Enemy";
                enemy.turnInBattle = true;
                enRoll = 0;
            }
            i++;
        }
        NextTurn();

    }
    public IEnumerator enemyCharacterAttack(Character player)
    {
        int damage = 0;
        int toHit = 0;

        if (enemy.hitStat == "strength")
            toHit = enemy.CalculateHitStrength();
        else if (enemy.hitStat == "dexterity")
            toHit = enemy.CalculateHitDexterity();

        if (toHit >= player.tempAC)
        {
            if (enemy.hitStat == "strength")
                damage = ((enemy.strength - 10) / 2) + UnityEngine.Random.Range(1, 5);
            else if (enemy.hitStat == "dexterity")
                damage = ((enemy.dexterity - 10) / 2) + UnityEngine.Random.Range(1, 5);
            eventText.text = enemy.fullName + " hits " + player.fullName + " for " + damage + " damage.";
            yield return new WaitForSeconds(2f);
            player.TakeDamage(damage);
            if (player.isAlive == false)
            {
                hud.setHP(yvonna, shen, ruth, enemy);
                eventText.text = player.fullName + " has died";
                yield return new WaitForSeconds(2f);
                StartCoroutine(CheckLoss());
            }
            else
            {
                hud.setHP(yvonna, shen, ruth, enemy);
                currentTurn++;
                NextTurn();
            }
        }
        else
        {
            eventText.text = enemy.fullName + " Misses";
            hud.setHP(yvonna, shen, ruth, enemy);
            yield return new WaitForSeconds(1.5f);
            currentTurn++;
            NextTurn();
        }
    }
    public IEnumerator playerCharacterAttack(Character player)
    {
        int damage = 0;
        int toHit = 0;

        if (player.hitStat == "strength")
            toHit = player.CalculateHitStrength();
        else if (player.hitStat == "dexterity")
            toHit = player.CalculateHitDexterity();

        if (toHit >= enemy.tempAC)
        {
            if (player.hitStat == "strength")
                damage = ((player.strength - 10) / 2) + UnityEngine.Random.Range(1, 11);
            else if (player.hitStat == "dexterity")
                damage = ((player.dexterity - 10) / 2) + UnityEngine.Random.Range(1, 9);
            eventText.text = player.fullName + " attacks " + enemy.fullName + " for " + damage + " damage";
            enemy.TakeDamage(damage);
            hud.setHP(yvonna, shen, ruth, enemy);
            yield return new WaitForSeconds(2f);
            if (enemy.isAlive == false)
            {
                yield return new WaitForSeconds(2f);
                StartCoroutine(WinBattle());
            }
            else
            {
                currentTurn++;
                NextTurn();
            }
        }
        else
        {
            eventText.text = player.fullName + " Misses";
            yield return new WaitForSeconds(1.5f);
            currentTurn++;
            NextTurn();
        }
    }
    public void playerMagic()
    {
        if (state != BattleState.YVONNATURN && state != BattleState.SHENTURN && state != BattleState.RUTHTURN)
            return;

        hud.magicPanel.SetActive(true);
        if(state == BattleState.SHENTURN)
        {
            hud.mtext1.text = "Shield";
            hud.mtext2.text = "Chill Touch";
            hud.thirdMagicButton.SetActive(false);
        }
        else if(state == BattleState.RUTHTURN)
        {
            hud.mtext1.text = "Shield of Faith";
            hud.mtext2.text = "Sacred Flame";
            hud.thirdMagicButton.SetActive(true);
            hud.mtext3.text = "Cure Wounds";
        }
        else if (state == BattleState.YVONNATURN)
        {
            eventText.text = "Yvonna doesn't know any Magic!";
            hud.magicPanel.SetActive(false);
        }
    }
    public void firstMagicButtonClick()
    {
        if (state == BattleState.SHENTURN && shen.currentMp > 0)
        {
            shen.tempAC += 6;
            eventText.text = "Shen casts Shield and gains 6 temporary Armor Class!";
            shen.currentMp -= 1;
            hud.magicPanel.SetActive(false);
            StartCoroutine(DoItAgain());
        }
        else if (state == BattleState.RUTHTURN && ruth.currentMp > 0)
        {
            yvonna.tempAC += 2;
            shen.tempAC += 2;
            ruth.tempAC += 2;
            eventText.text = "Ruth casts Shield of Faith and the party gains 2 temporary Armor Class each!";
            ruth.currentMp -= 1;
            StartCoroutine(DoItAgain());
        }
        else
        {
            eventText.text = "That character doesn't have enough MP!";
        }
    }
    public void secondMagicButtonClick()
    {
        int damage = 0;
        if (state == BattleState.SHENTURN && shen.currentMp > 0)
        {
            damage = shen.proficiency + UnityEngine.Random.Range(1, 9);
            enemy.TakeDamage(damage);
            eventText.text = "Shen casts Chill Touch and freezes " + enemy.fullName + " for " + damage + " damage!";
            shen.currentMp -= 1;
            hud.magicPanel.SetActive(false);
            StartCoroutine(DoItAgain());
        }
        else if (state == BattleState.RUTHTURN && ruth.currentMp > 0)
        {
            damage = ruth.proficiency + UnityEngine.Random.Range(1, 9);
            enemy.TakeDamage(damage);
            eventText.text = "Ruth casts Sacred Flame and burns " + enemy.fullName + " for " + damage + " damage!";
            ruth.currentMp -= 1;
            hud.magicPanel.SetActive(false);
            StartCoroutine(DoItAgain());
        }
        else
        {
            eventText.text = "That character doesn't have enough MP!";
        }
    }
    public void thirdMagicButtonClick()
    {
        int damage = 0;
        if (state == BattleState.SHENTURN && shen.currentMp > 0)
        {
            eventText.text = "Shen doesn't know that spell.";
        }
        else if (state == BattleState.RUTHTURN && ruth.currentMp > 0)
        {
            damage = ruth.proficiency + UnityEngine.Random.Range(1, 7);
            yvonna.HealDamage(damage);
            shen.HealDamage(damage);
            ruth.HealDamage(damage);
            eventText.text = "Ruth casts Cure Wounds and heals the whole party for " + damage + " HP!";
            ruth.currentMp -= 1;
            hud.magicPanel.SetActive(false);
            StartCoroutine(DoItAgain());
        }
        else
        {
            eventText.text = "That character doesn't have enough MP!";
        }
    }
    public void backMagicButtonClick()
    {
        hud.magicPanel.SetActive(false);
    }
    public IEnumerator DoItAgain()
    { 
        hud.magicPanel.SetActive(false);
        yield return new WaitForSeconds(2f);
        hud.setHP(yvonna, shen, ruth, enemy);
        hud.setMP(yvonna, shen, ruth);
        currentTurn++;
        NextTurn();
    }
    public void NextTurn()
    {
        // Makes sure the currentTurn is never out of range for the turnOrder array
        UnityEngine.Debug.Log("Next Turn");
        hud.magicPanel.SetActive(false);
        if (currentTurn >= 4)
            currentTurn = 0;
        hud.defaultImage();

        // Determines who's turn it is by checking the turn order array and seeing if the index of the currentTurn is equal to a character's name
        if (turnOrder[currentTurn] == "Yvonna")
        {
            if (yvonna.currentHp != 0)
            {
                state = BattleState.YVONNATURN;
                eventText.text = "Yvonna's Turn.";
                hud.yvonnaTurn.SetActive(true);
                hud.yvonnaNeutral.SetActive(false);
            }
            else
            {
                currentTurn++;
                NextTurn();
            }
        }
        else if (turnOrder[currentTurn] == "Shen")
        {
            if (shen.currentHp != 0)
            {
                state = BattleState.SHENTURN;
                eventText.text = "Shen's Turn.";
                hud.shenTurn.SetActive(true);
                hud.shenNeutral.SetActive(false);
            }
            else
            {
                currentTurn++;
                NextTurn();
            }
        }
        else if (turnOrder[currentTurn] == "Ruth")
        {
            if (ruth.currentHp != 0)
            {
                state = BattleState.RUTHTURN;
                eventText.text = "Ruth's Turn.";
                hud.ruthTurn.SetActive(true);
                hud.ruthNeutral.SetActive(false);
            }
            else
            {
                currentTurn++;
                NextTurn();
            }
        }
        else if(turnOrder[currentTurn] == "Enemy")
        {
            if (enemy.currentHp != 0)
            {
                state = BattleState.ENEMYTURN;
                eventText.text = "Enemy's Turn.";
                hud.enemyTurn.SetActive(true);
                hud.enemyNeutral.SetActive(false);
                StartCoroutine(EnemyAttack());
            }
            else
            {
                currentTurn++;
                NextTurn();
            }
        } 
    }
    public void ResetStats()
    {
        // Resets all character variable stats to their defaults

        yvonna.currentHp = yvonna.maxHp;
        shen.currentHp = shen.maxHp;
        ruth.currentHp = ruth.maxHp;
        enemy.currentHp = enemy.maxHp;

        yvonna.tempAC = yvonna.armorClass;
        shen.tempAC = shen.armorClass;
        ruth.tempAC = ruth.armorClass;
        enemy.tempAC = enemy.armorClass;

        yvonna.currentMp = yvonna.maxMp;
        shen.currentMp = shen.maxMp;
        ruth.currentMp = ruth.maxMp;
        enemy.currentMp = enemy.maxMp;

        yvonna.isAlive = true;
        shen.isAlive = true;
        ruth.isAlive = true;
        enemy.isAlive = true;

        yvonna.turnInBattle = false;
        shen.turnInBattle = false;
        ruth.turnInBattle = false;
        enemy.turnInBattle = false;

        UnityEngine.Debug.Log("Reset Stats");

    }
    public IEnumerator CheckLoss()
    {
        // Checks to see if the isAlive bool is false for all player characters. If so the player loses, otherwise, go to the next turn.
        if (yvonna.isAlive == false && shen.isAlive == false && ruth.isAlive == false)
        {
            state = BattleState.LOSE;
            eventText.text = "You Lose.";
            yield return new WaitForSeconds(3f);
            ResetStats();
            player.SetActive(true);
            SceneManager.LoadScene("LevelOne");
        }
        else
        {
            currentTurn++;
            NextTurn();
        }
    }
    public IEnumerator WinBattle()
    {
        // The player wins and the battle ends
        state = BattleState.WIN;
        eventText.text = "The battle is won!";

        yield return new WaitForSeconds(3f);
        hud.backgroundPanel.SetActive(false);
        hud.fightHud.SetActive(false);
        player.SetActive(true);
    }
}
