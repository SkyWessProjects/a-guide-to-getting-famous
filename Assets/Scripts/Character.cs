﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class Character : ScriptableObject
{
    public string fullName;
    public Sprite portrait;
    public string hitStat;
    public bool turnInBattle = false;

    public bool isAlive = true;
    public int maxHp;
    public int currentHp;

    public int maxMp;
    public int currentMp;

    public int armorClass;
    public int tempAC;
    public int initiative;
    public int proficiency;

    public int strength;
    public int constitution;
    public int dexterity;
    public int itelligence;
    public int wisdom;
    public int charisma;

    public void HealDamage(int damage)
    {
        currentHp = currentHp + damage;
        if (currentHp >= 0)
            isAlive = true;
    }
    public void TakeDamage(int damage)
    {
        currentHp = currentHp - damage;
        if (currentHp <= 0)
            isAlive = false;
    }
    public void UseMP(int mpCost)
    {
        currentMp -= mpCost;
    }
    public int CalculateHitStrength()
    {
        int sRoll = Random.Range(1, 21);
        int hit = sRoll + ((strength - 10) / 2) + proficiency;
        return hit;
    }
    public int CalculateHitDexterity()
    {
        int sRoll = Random.Range(1, 21);
        int hit = sRoll + ((dexterity - 10) / 2) + proficiency;
        return hit;
    }
}
