﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleHud : MonoBehaviour
{
    public TextMeshProUGUI yvonnaHP;
    public TextMeshProUGUI shenHP;
    public TextMeshProUGUI ruthHP;
    public TextMeshProUGUI yvonnaMP;
    public TextMeshProUGUI shenMP;
    public TextMeshProUGUI ruthMP;

    public TextMeshProUGUI enemyName;
    public TextMeshProUGUI enemyHP;

    public GameObject yvonnaNeutral;
    public GameObject yvonnaTurn;
    public GameObject shenNeutral;
    public GameObject shenTurn;
    public GameObject ruthNeutral;
    public GameObject ruthTurn;
    public GameObject enemyNeutral;
    public GameObject enemyTurn;

    public GameObject backgroundPanel;
    public GameObject fightHud;

    public GameObject magicPanel;
    public TextMeshProUGUI mtext1;
    public TextMeshProUGUI mtext2;
    public TextMeshProUGUI mtext3;
    public GameObject thirdMagicButton;

    public void setHP(Character yvonna, Character shen, Character ruth, Character enemy)
    {
        if (yvonna.currentHp < 0)
            yvonna.currentHp = 0;
        if (yvonna.currentHp > yvonna.maxHp)
            yvonna.currentHp = yvonna.maxHp;

        if (shen.currentHp < 0)
            shen.currentHp = 0;
        if (shen.currentHp > shen.maxHp)
            shen.currentHp = shen.maxHp;

        if (ruth.currentHp < 0)
            ruth.currentHp = 0;
        if (ruth.currentHp > ruth.maxHp)
            ruth.currentHp = ruth.maxHp;


        if (enemy.currentHp < 0)
            enemy.currentHp = 0;
        if (enemy.currentHp > enemy.maxHp)
            enemy.currentHp = enemy.maxHp;

        yvonnaHP.text = "HP: " + yvonna.currentHp + "/" + yvonna.maxHp;
        shenHP.text = "HP: " + shen.currentHp + "/" + shen.maxHp;
        ruthHP.text = "HP: " + ruth.currentHp + "/" + ruth.maxHp;
        enemyHP.text = "HP: " + enemy.currentHp + "/" + enemy.maxHp;
    }
    public void setMP(Character yvonna, Character shen, Character ruth)
    {
        yvonnaMP.text = "MP: " + yvonna.currentMp + "/" + yvonna.maxMp;
        shenMP.text = "MP: " + shen.currentMp + "/" + shen.maxMp;
        ruthMP.text = "MP: " + ruth.currentMp + "/" + ruth.maxMp;
    }
    public void setEName(Character enemy)
    {
        enemyName.text = enemy.fullName;
    }
    public void defaultImage()
    {
        yvonnaNeutral.SetActive(true);
        yvonnaTurn.SetActive(false);
        shenNeutral.SetActive(true);
        shenTurn.SetActive(false);
        ruthNeutral.SetActive(true);
        ruthTurn.SetActive(false);
        enemyNeutral.SetActive(true);
        enemyTurn.SetActive(false);
    }
}
