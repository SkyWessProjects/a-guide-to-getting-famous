﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogDisplay2 : MonoBehaviour
{
    public BattleSystem BSystem;
    public Conversation conversation2;
    public GameObject speaker;
    public GameObject player;
    public bool hasAFight;

    private SpeakerUI speakerUI;

    private int activeLineIndex = 0;
    private bool inRange = false;

    public void StartConvo()
    {
        speakerUI = speaker.GetComponent<SpeakerUI>();
        speakerUI.Speaker = conversation2.speaker;
        speaker.SetActive(true);
    }
    private void Update()
    {
        if (Input.GetKeyDown("space") && speaker.activeInHierarchy == false && inRange == true)
        {
            StartConvo();
        }
        if (Input.GetKeyDown("space") && speaker.activeInHierarchy == true)
        {
            AdvanceConversation();
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Enter collider");
        if (other.CompareTag("Player"))
        {
            inRange = true;
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Exit collider");
        if (other.CompareTag("Player"))
        {
            inRange = false;
        }
    }
    public void AdvanceConversation()
    {
        if (activeLineIndex < conversation2.lines.Length)
        {
            DisplayLine();
            activeLineIndex += 1;
        }
        else
        {
            activeLineIndex = 0;
            speakerUI.hide();

            if (BSystem && hasAFight == true)
            {
                BSystem.BeginBattle();
            }
        }

    }
    void DisplayLine()
    {
        Line line = conversation2.lines[activeLineIndex];
        Character character = line.character;

        if (speakerUI.SpeakerIs(character))
        {
            SetDialog(speakerUI, line.text, character);
        }
        else
        {
            speakerUI.ChangeSpeaker(character);
            SetDialog(speakerUI, line.text, character);
        }
    }
    void SetDialog(SpeakerUI activeSpeakerUI, string text, Character character)
    {
        activeSpeakerUI.Dialog = text;
        activeSpeakerUI.show();
    }
}
