﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class FollowScript : MonoBehaviour
{
    public float speed;
    public GameObject followE;
    protected Transform target;
    public Animator animator;
    protected Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        target = followE.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        FollowT();
    }
    public void FollowT()
    {
        if (Vector2.Distance(transform.position, target.position) > 1 && target != null)
        {
            direction = (target.transform.position - transform.position).normalized;
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            animator.SetFloat("Horizontal", direction.x);
            animator.SetFloat("Vertical", direction.y);
            animator.SetFloat("Speed", direction.sqrMagnitude);
        }
        else
        {

            animator.SetFloat("Speed", 0);
        }
    }
}