﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SpeakerUI : MonoBehaviour
{
    public Image portrait;
    public TextMeshProUGUI fullName;
    public TextMeshProUGUI dialog;

    public Character speaker;
    public Character Speaker
    {
        get { return speaker; }
        set
        {
            speaker = value;
            portrait.sprite = speaker.portrait;
            fullName.text = speaker.fullName;
        }
    }
    public string Dialog
    {
        set { dialog.text = value; }
    }
    public bool HasSpeaker()
    {
        return speaker != null;
    }
    public bool SpeakerIs(Character character)
    {
        return speaker == character;
    }
    public void show()
    {
        gameObject.SetActive(true);
    }
    public void hide()
    {
        gameObject.SetActive(false);
    }
    public void ChangeSpeaker(Character character)
    {
        portrait.sprite = character.portrait;
        fullName.text = character.fullName;
    }
}
