﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTrigger : MonoBehaviour
{
    public GameObject winPanel;
    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Enter collider");
        if (other.CompareTag("Player"))
        {
            winPanel.SetActive(true);
        }
    }
    public void Quit()
    {
        Application.Quit();
        UnityEngine.Debug.Log("Quit");
    }
}