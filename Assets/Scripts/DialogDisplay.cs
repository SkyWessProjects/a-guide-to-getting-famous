﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogDisplay : MonoBehaviour
{
    public BattleSystem BSystem;
    public Conversation convo;
    public Conversation conversation;
    public GameObject speaker;
    public GameObject player;

    private SpeakerUI speakerUI;

    private int activeLineIndex = 0;
    private bool inRange = false;
    private void Awake()
    {
        conversation = convo;
    }
    private void Update()
    {
        if (Input.GetKeyDown("space") && speaker.activeInHierarchy == false && inRange == true)
        {
            conversation.hasPlayed = true;
            speaker.SetActive(true);
            speakerUI = speaker.GetComponent<SpeakerUI>();
            speakerUI.Speaker = conversation.speaker;
        }
        if (Input.GetKeyDown("space") && speaker.activeInHierarchy == true)
        {
            AdvanceConversation();
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Enter collider");
        if (other.CompareTag("Player"))
        {
            inRange = true;
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Exit collider");
        if (other.CompareTag("Player"))
        {
            inRange = false;
        }
    }
    public void AdvanceConversation()
    {
        if (activeLineIndex < conversation.lines.Length)
        {
            DisplayLine();
            activeLineIndex += 1;
        }
        else
        {
            activeLineIndex = 0;
            speakerUI.hide();
            if (BSystem)
            {
                BSystem.BeginBattle();
            }
        }

    }
    void DisplayLine()
    {
        Line line = conversation.lines[activeLineIndex];
        Character character = line.character;

        if(speakerUI.SpeakerIs(character))
        {
            SetDialog(speakerUI, line.text, character);
        }
        else
        {
            speakerUI.ChangeSpeaker(character);
            SetDialog(speakerUI, line.text, character);
        }
    }
    void SetDialog(SpeakerUI activeSpeakerUI, string text, Character character)
    {
        activeSpeakerUI.Dialog = text;
        activeSpeakerUI.show();
    }
}
    